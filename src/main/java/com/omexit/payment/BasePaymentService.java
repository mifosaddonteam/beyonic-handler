package com.omexit.payment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omexit.beyonic.BeyonicHelper;
import com.omexit.util.HttpClientHelper;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public abstract class BasePaymentService {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	public BeyonicHelper createBeyonicHelper(String urlEndpoint, String token) {

		Retrofit retrofit = new Retrofit.Builder().baseUrl(urlEndpoint)
				.addConverterFactory(JacksonConverterFactory.create())
				.client(HttpClientHelper.getUnsafeOkHttpClientToken(token)).build();

		logger.info(String.format("createBeyonicHelper - (%s)", urlEndpoint));

		return retrofit.create(BeyonicHelper.class);
	}
}
