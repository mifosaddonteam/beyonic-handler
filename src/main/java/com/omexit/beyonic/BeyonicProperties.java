package com.omexit.beyonic;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Antony on 2/11/2016.
 */
@ConfigurationProperties(prefix = "beyonic")
@Component
public class BeyonicProperties {
    private String endPoint;
    private String token;
    private String callbackUrl;
    private String mifosPaymentBridgeCallbackUrl;

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getMifosPaymentBridgeCallbackUrl() {
        return mifosPaymentBridgeCallbackUrl;
    }

    public void setMifosPaymentBridgeCallbackUrl(String mifosPaymentBridgeCallbackUrl) {
        this.mifosPaymentBridgeCallbackUrl = mifosPaymentBridgeCallbackUrl;
    }
}
