package com.omexit.beyonic;

import com.omexit.beyonic.portfolio.BeyonicPaymentResponse;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Part;

/**
 * Created by Antony on 2/15/2016.
 */
public interface BeyonicHelper {
	@FormUrlEncoded
    @GET("payments")
    Call<BeyonicPaymentResponse> sendNewPayment(@Part("phonenumber") String phoneNumber,
                                         @Part("currency") String currency,
                                         @Part("amount") String amount,
                                         @Part("description") String description,
                                         @Part("callback_url") String callbackUrl,
                                         @Part("payment_type") String paymentType);
}
